/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.clipboard;

import djf.components.AppClipboardComponent;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.Circle_LogoItem;
import gologolo.data.logoitems.Image_LogoItem;
import gologolo.data.logoitems.LogoItem;
import gologolo.data.logoitems.LogoItemTypes;
import gologolo.data.logoitems.Rectangle_LogoItem;
import gologolo.data.logoitems.Text_LogoItem;
import gologolo.transactions.DeleteLogoItem_Transaction;
import gologolo.workspace.GoLogoLoWorkspace;
import gologolo.workspace.controllers.ItemsController;
import javafx.scene.Node;

/**
 *
 * @author doov
 */
public class GoLogoLoClipboard implements AppClipboardComponent {
    private GoLogoLoApp app;
    Node copiedItem;
    
    public GoLogoLoClipboard(GoLogoLoApp initapp){
        this.app = initapp;
        copiedItem = null;
    }

    @Override
    public void cut() {
        if(hasSomethingToCut()){
            GoLogoLoData data = ((GoLogoLoData)app.getDataComponent());
            copiedItem = ((LogoItem)data.getSelectedItem()).clone();
            DeleteLogoItem_Transaction transaction = new DeleteLogoItem_Transaction(data, data.getSelectedItem());
            app.processTransaction(transaction);
                    
        }
    }

    @Override
    public void copy() {
        if(hasSomethingToCopy()){
            GoLogoLoData data = ((GoLogoLoData)app.getDataComponent());
            copiedItem = ((LogoItem)data.getSelectedItem()).clone();
        }
    }

    @Override
    public void paste() {
        if(hasSomethingToPaste()){
            ItemsController itemController = ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getItemController();
            if(((LogoItem)copiedItem).getItemType().equalsIgnoreCase(LogoItemTypes.RECTANGLE_ITEM.toString())){
                itemController.addRectangle(((Rectangle_LogoItem)copiedItem).clone());
            } else if(((LogoItem)copiedItem).getItemType().equalsIgnoreCase(LogoItemTypes.TEXT_ITEM.toString())){
                itemController.addText(((Text_LogoItem)((Text_LogoItem)copiedItem).clone()));
            } else if(((LogoItem)copiedItem).getItemType().equalsIgnoreCase(LogoItemTypes.IMAGE_ITEM.toString())){
                itemController.addImage((Image_LogoItem) ((Image_LogoItem)copiedItem).clone());
            } else if(((LogoItem)copiedItem).getItemType().equalsIgnoreCase(LogoItemTypes.CIRCLE_ITEM.toString())){
                itemController.addCircle((Circle_LogoItem) ((Circle_LogoItem)copiedItem).clone());
            }
        }
    }

    @Override
    public boolean hasSomethingToCut() {
        return ((GoLogoLoData)app.getDataComponent()).isItemSelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((GoLogoLoData)app.getDataComponent()).isItemSelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        boolean hasSomething = true;
        if(copiedItem == null){
            hasSomething = false;
        }
        return hasSomething;
    }
}
