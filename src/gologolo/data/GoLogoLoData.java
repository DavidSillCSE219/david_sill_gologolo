/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_CANVAS;
import gologolo.data.logoitems.LogoItem;
import gologolo.data.logoitems.Rectangle_LogoItem;
import gologolo.workspace.GoLogoLoWorkspace;
import gologolo.workspace.controllers.DragController;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author doov
 */
public class GoLogoLoData implements AppDataComponent{
    private GoLogoLoApp app;
    ObservableList<Node> logoItems;
    Node selectedItem;
    DragController dragController;
    
    public GoLogoLoData(GoLogoLoApp initapp){
        this.app = initapp;
        dragController = ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getItemController().getDragController();
        
    }
    @Override
    public void reset() {
        AppGUIModule gui = app.getGUIModule();
        TableView table = ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getTable();
//        if(logoItems == null){
//            logoItems = table.getItems();
//        } else {
//            logoItems.clear();
//        }
        logoItems = FXCollections.observableArrayList(new ArrayList<Node>());
        selectedItem = null;
        ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getLogoFoolProofDesign().updateControls();
        
    }

    public ObservableList<Node> getLogoItems() {
        return logoItems;
    }

    public Node getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Node selectedItem) {
        clearSelected();
        this.selectedItem = selectedItem;
        ((LogoItem)this.selectedItem).highlight();
        app.getFoolproofModule().updateAll();
        ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getLogoFoolProofDesign().updateControls();
    }
    
    public void clearSelected() {
        if(this.selectedItem != null){
            ((LogoItem)this.selectedItem).removeHighlight();
            this.selectedItem = null;
        }
        app.getFoolproofModule().updateAll();
        ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getLogoFoolProofDesign().updateControls();
    }
    
    public void addItem(Node item){
        logoItems.add(item);
        dragController.makeDraggable(item);
        ((GoLogoLoWorkspace)app.getWorkspaceComponent()).updateCanvas();
    }
    
    public void removeItem(Node item){
        logoItems.remove(item);
        ((GoLogoLoWorkspace)app.getWorkspaceComponent()).updateCanvas();
    }
    
    public boolean isItemSelected() {
        boolean selected = true;
        if(selectedItem == null){
            selected = false;
        }
        return selected;
    }
    
    public Iterator<Node> getIterator(){
        return this.logoItems.iterator();
    }   
    
    public Node getWorkPane(){
        return app.getGUIModule().getGUINode(LOGO_CANVAS);
    }
    
    public void moveItem(int newIndex, Node item){
        if(item != null && newIndex >= 0){
            logoItems.remove(item);
            logoItems.add(newIndex, item);
            ((GoLogoLoWorkspace)app.getWorkspaceComponent()).updateCanvas();
        }
        
    }
    
    public void moveItemDown(){
        if(selectedItem != null){
            int newIndex = logoItems.indexOf(selectedItem) - 1;
            if (newIndex >= 0) {
                logoItems.remove(selectedItem);
                logoItems.add(newIndex, selectedItem);
                ((GoLogoLoWorkspace)app.getWorkspaceComponent()).updateCanvas();
            }
        }
    }
    
    public void moveItemUp(){
        if(selectedItem != null){
            int newIndex = logoItems.indexOf(selectedItem) + 1;
            logoItems.remove(selectedItem);
            logoItems.add(newIndex, selectedItem);
            ((GoLogoLoWorkspace)app.getWorkspaceComponent()).updateCanvas();
        }
    }
}
