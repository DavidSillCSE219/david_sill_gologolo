/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;

/**
 *
 * @author doov
 */
public interface LogoItem {
    
    static public double DEFAULT_SIZE = 50.0;
    static public double DEFAULT_BORDER = 2.0;
    
    abstract void highlight();
    abstract void removeHighlight();
    abstract void delete();
    abstract Node clone();
    abstract String getItemType();
    abstract String getName();
    abstract void setName(String name);
    abstract boolean hasTextOptions();
    abstract boolean hasBorderOptions();
    abstract void setItemSize(double Size);
    abstract double getItemSize();
    abstract void setBorderColor(Color color);
    abstract Color getBorderColor();
    abstract void setBorderRadius(double radius);
    abstract double getBorderRadius();
    abstract void setBorderThickness(double thickness);
    abstract double getBorderThickness();
    abstract boolean hasColorGradientOptions();
}
