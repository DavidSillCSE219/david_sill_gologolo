/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author doov
 */
public interface GradientOptions {
    
    public static RadialGradient DEFAULT_GRADIENT = new RadialGradient(0, 0, 0.5, 0.5, 1, true, CycleMethod.NO_CYCLE, new Stop[] {
        new Stop(0, Color.RED),
        new Stop(1, Color.BLUE)
    });
    
     public static RadialGradient OTHER_GRADIENTS = new RadialGradient(0, 0, 0.5, 0.5, 1, true, CycleMethod.NO_CYCLE, new Stop[] {
        new Stop(0, Color.YELLOW),
        new Stop(1, Color.GREEN)
    });
    
    abstract void setRadialGradient(RadialGradient gradient);
    abstract RadialGradient getRadialGradient();
    
    
}
