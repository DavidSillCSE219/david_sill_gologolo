/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import static gologolo.data.logoitems.LogoItemTypes.RECTANGLE_ITEM;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_HIGHLIGHT;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author doov
 */
public class Rectangle_LogoItem extends Rectangle implements LogoItem, GradientOptions {
    
    
    private double borderThickness;
    private double borderRadius;
    private Color  borderColor;
    private RadialGradient colorGradient;
    private String name;
    static int numOfRectangles;
    public final String ITEM_TYPE = RECTANGLE_ITEM.toString();
    final boolean TEXT_OPTIONS = false;
    final boolean BORDER_OPTIONS = true;
    final boolean COLOR_GRADIENT_OPTIONS = true;
    Circle vertex;

    

    public Rectangle_LogoItem() {
        this(DEFAULT_BORDER, 0.0, Color.BLACK, DEFAULT_GRADIENT, "Rectangle_" + numOfRectangles+1);
        vertex = new Circle(10.0, Color.GOLD);
    } 
    
    public Rectangle_LogoItem(double borderThickness, double borderRadius, Color borderColor, RadialGradient colorGradient, String name) {
        super(0.0, 0.0, DEFAULT_SIZE, DEFAULT_SIZE);
        setBorderThickness(borderThickness);
        setBorderRadius(borderRadius);
        setBorderColor(borderColor);
        setRadialGradient(colorGradient);
        setName(name);
        numOfRectangles++;
       
    }
    
    public void setBorderThickness(double borderThickness) {
        this.borderThickness = borderThickness;
        super.setStrokeWidth(borderThickness);
    }
    @Override
    public void setBorderRadius(double borderRadius) {
        this.borderRadius = borderRadius;
        super.setArcWidth(borderRadius);
        super.setArcHeight(borderRadius);
    }

    @Override
    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
        super.setStroke(borderColor);
    }
    
    public double getBorderThickness() {
        return borderThickness;
    }

    @Override
    public double getBorderRadius() {
        return borderRadius;
    }

    @Override
    public Color getBorderColor() {
        return borderColor;
    }

    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
         
    }

    public String getITEM_TYPE() {
        return ITEM_TYPE;
    }
    
    @Override
    public void highlight() {
        this.getStyleClass().add(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void removeHighlight() {
        this.getStyleClass().remove(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void delete() {
        numOfRectangles--;
    }
    @Override
    public Rectangle_LogoItem clone(){
       Rectangle_LogoItem rect = new Rectangle_LogoItem(borderThickness, borderRadius, borderColor, colorGradient, name);
       rect.setFill(this.getFill());
       rect.setItemSize(this.getItemSize());
       return rect;
       
    }

    @Override
    public String getItemType() {
        return LogoItemTypes.RECTANGLE_ITEM.toString();
    }

    @Override
    public boolean hasTextOptions() {
        return TEXT_OPTIONS;
    }

    @Override
    public boolean hasBorderOptions() {
        return BORDER_OPTIONS;
    }

    @Override
    public boolean hasColorGradientOptions() {
        return COLOR_GRADIENT_OPTIONS;
    }

    @Override
    public void setItemSize(double Size) {
        this.setWidth(Size);
        this.setHeight(Size);
    }

    @Override
    public double getItemSize() {
        return this.getWidth();
    }

    @Override
    public void setRadialGradient(RadialGradient gradient) {
        this.colorGradient = gradient;
        super.setFill(gradient);
    }

    @Override
    public RadialGradient getRadialGradient() {
        return this.colorGradient;
    }
    
    
    
}
