/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import static gologolo.data.logoitems.LogoItemTypes.IMAGE_ITEM;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_HIGHLIGHT;
import java.io.File;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

/**
 *
 * @author doov
 */
public class Image_LogoItem extends ImageView implements LogoItem{
    private String name;
    static int numOfImages;
    public final String ITEM_TYPE = IMAGE_ITEM.toString();
    final boolean TEXT_OPTIONS = false;
    final boolean BORDER_OPTIONS = false;
    final boolean COLOR_GRADIENT_OPTIONS = false;
    Image image;
    File imageFile;


    public Image_LogoItem(Image image, File file) {
        this.image = image;
        this.name = "Image_" + ++numOfImages;
        this.imageProperty().set(this.image);
        this.imageFile = file;
    }    
    

    @Override
    public void highlight() {
        this.getStyleClass().add(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void removeHighlight() {
        this.getStyleClass().remove(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void delete() {
        numOfImages--;
    }

    @Override
    public Node clone() {
        return new Image_LogoItem(image, imageFile);
    }

    @Override
    public String getItemType() {
        return this.ITEM_TYPE;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean hasTextOptions() {
        return TEXT_OPTIONS;
    }

    @Override
    public boolean hasBorderOptions() {
        return BORDER_OPTIONS;
    }

    @Override
    public void setBorderColor(Color color) {
        
    }

    @Override
    public Color getBorderColor() {
        return null;
    }

    @Override
    public void setBorderRadius(double radius) {
        
    }

    @Override
    public double getBorderRadius() {
        return -1.0;
    }

    @Override
    public void setBorderThickness(double thickness) {
        
    }

    @Override
    public double getBorderThickness() {
        return -1.0;
    }

    @Override
    public boolean hasColorGradientOptions() {
        return COLOR_GRADIENT_OPTIONS;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    @Override
    public void setItemSize(double Size) {
        
    }

    @Override
    public double getItemSize() {
        return -1.0;
    }
    
    
    
}
