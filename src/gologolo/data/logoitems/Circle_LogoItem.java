/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import static gologolo.data.logoitems.LogoItemTypes.CIRCLE_ITEM;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_HIGHLIGHT;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Circle;

/**
 *
 * @author doov
 */
public class Circle_LogoItem extends Circle implements LogoItem, GradientOptions
{
    private double borderThickness;
    private Color  borderColor;
    private RadialGradient colorGradient;
    private String name;
    static int numOfCircles;
    public final String ITEM_TYPE = CIRCLE_ITEM.toString();
    final boolean TEXT_OPTIONS = false;
    final boolean BORDER_OPTIONS = true;
    final boolean COLOR_GRADIENT_OPTIONS = true;

    public Circle_LogoItem() {
        this(DEFAULT_BORDER, Color.BLACK, "Circle_" + numOfCircles +1);
    }

    public Circle_LogoItem(double borderThickness, Color borderColor, String name) {
        super(DEFAULT_SIZE);
        setRadialGradient(DEFAULT_GRADIENT);
        setBorderThickness(borderThickness);
        setBorderColor(borderColor);
        setName(name);
        numOfCircles++;
        this.setTranslateX(this.getTranslateX() + DEFAULT_SIZE);
        this.setTranslateY(this.getTranslateY() + DEFAULT_SIZE);
        
    }
    

    @Override
    public void highlight() {
        this.getStyleClass().add(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void removeHighlight() {
        this.getStyleClass().remove(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void delete() {
        numOfCircles--;
    }

    @Override
    public Node clone() {
        Circle_LogoItem circle = new Circle_LogoItem(borderThickness, borderColor, name);
        circle.setItemSize(this.getItemSize());
        circle.setRadialGradient(this.colorGradient);
        return circle;
    }

    @Override
    public String getItemType() {
        return this.ITEM_TYPE.toString();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean hasTextOptions() {
        return this.TEXT_OPTIONS;
    }

    @Override
    public boolean hasBorderOptions() {
        return this.BORDER_OPTIONS;
    }

    @Override
    public void setBorderColor(Color color) {
        this.borderColor = color;
        this.setStroke(color);
    }

    @Override
    public Color getBorderColor() {
        return this.borderColor;
    }

    @Override
    public void setBorderRadius(double radius) {
        
    }

    @Override
    public double getBorderRadius() {
        return -1.0;
    }

    @Override
    public void setBorderThickness(double thickness) {
        this.borderThickness = thickness;
        this.setStrokeWidth(thickness);
    }

    @Override
    public double getBorderThickness() {
        return this.borderThickness;
    }

    @Override
    public boolean hasColorGradientOptions() {
        return this.COLOR_GRADIENT_OPTIONS;
    }

    @Override
    public void setItemSize(double Size) {
        this.setRadius(Size);
    }

    @Override
    public double getItemSize() {
        return this.getRadius();
    }

    @Override
    public void setRadialGradient(RadialGradient gradient) {
        this.colorGradient = gradient;
        this.setFill(gradient);
    }

    @Override
    public RadialGradient getRadialGradient() {
        return colorGradient;
    }
    
}
