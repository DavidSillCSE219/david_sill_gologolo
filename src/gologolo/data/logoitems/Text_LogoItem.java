/*
 * To change this license header, choose License Headers in Project Properties.S
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data.logoitems;

import static gologolo.data.logoitems.LogoItemTypes.TEXT_ITEM;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BOLD;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_HIGHLIGHT;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_ITALIC;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author doov
 */
public class Text_LogoItem extends Label implements LogoItem{

    public final String ITEM_TYPE = TEXT_ITEM.toString();
    public String name;
    boolean bold = false;
    boolean italic = false;
    boolean underline = false;
    int fontSize;
    String fontFamily;
    public static String DEFAULT_TEXT = "New Text!";
    final boolean TEXT_OPTIONS = true;
    final boolean BORDER_OPTIONS = false;
    final boolean COLOR_GRADIENT_OPTIONS = false;
    final String CSS_FONT_SIZE   = "-fx-font-size: ";
    final String CSS_FONT_FAMILY = "-fx-font-family: ";
    final String CSS_BOLD        = "-fx-font-weight: bold;";
    final String CSS_ITALIC      = "-fx-font-style: italic;";
    final String CSS_UNDERLINE   = "-fx-underline: true;";
    final String CSS_END         = ";";
    final String CSS_NEW_LINE    = "\n";
    

    public Text_LogoItem() {
        this(DEFAULT_TEXT);
        
        
    }
    
    public Text_LogoItem(String text) {
        super(text);
        if(text.length() >= 6){
            name = text.substring(0, 6) + "_TEXT";
        } else {
            name = text + "_TEXT";
        }
        this.fontSize = this.getFontSize();
        this.fontFamily = this.getFont().getFamily();
    }

    @Override
    public void highlight() {
        this.getStyleClass().add(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void removeHighlight() {
        this.getStyleClass().remove(CLASS_LOGO_HIGHLIGHT);
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node clone() {
        Text_LogoItem clone = new Text_LogoItem(this.getText());
        clone.setFontFamily(this.fontFamily);
        clone.setFontSize(this.fontSize);
        if(isBold()){ clone.toggleBold();}
        if(isItalic()){ clone.toggleItalic();}
        if(isUnderlined()){ clone.toggleUnderline();}
        return clone;
    }

    @Override
    public String getItemType() {
        return this.ITEM_TYPE;
    }

    @Override
    public boolean hasTextOptions() {
        return TEXT_OPTIONS;
    }

    @Override
    public boolean hasBorderOptions() {
        return BORDER_OPTIONS;
    }

    @Override
    public boolean hasColorGradientOptions() {
        return COLOR_GRADIENT_OPTIONS;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setBorderColor(Color color) {
        
    }

    @Override
    public Color getBorderColor() {
        return null;
    }

    @Override
    public void setBorderRadius(double radius) {
        
    }

    @Override
    public double getBorderRadius() {
        return -1.0;
    }

    @Override
    public void setBorderThickness(double radius) {

    }

    @Override
    public double getBorderThickness() {
        return -1.0;
    }
    
    public void toggleBold(){
        bold = !bold;
        setCSS();
    }
    
    public boolean isBold(){
        return this.bold;
    }
    
    public void toggleItalic(){
        this.italic = !italic;
        setCSS();
    }
    
    public boolean isItalic(){
        return this.italic;
    }
    
    public void toggleUnderline(){
        this.underline = !underline;
        setCSS();
    }
    
    public boolean isUnderlined(){
        return this.underline;
    }
    
    public void setFontFamily(String family){
        this.fontFamily = family;
        setCSS();
    }
    
    public String getFontFamily(){
        return this.fontFamily;
    }
    
    public void setFontSize(int size){
        this.fontSize = size;
        setCSS();
    }
    
    public int getFontSize(){
        return (int) this.getFont().getSize();
    }
    
    public void setCSS(){
        StringBuilder sb = new StringBuilder();
        if(isBold()){
            sb.append(CSS_BOLD + CSS_NEW_LINE);
        }
        if(isItalic()){
            sb.append(CSS_ITALIC + CSS_NEW_LINE);
        }
        if(isUnderlined()){
            sb.append(CSS_UNDERLINE + CSS_NEW_LINE);
        }
        sb.append(CSS_FONT_FAMILY + this.fontFamily + CSS_END + CSS_NEW_LINE);
        sb.append(CSS_FONT_SIZE + this.fontSize + CSS_END + CSS_NEW_LINE);

        this.setStyle(sb.toString());
    }

    @Override
    public void setItemSize(double Size) {
        
    }

    @Override
    public double getItemSize() {
        return -1.0;
    }
    
}
