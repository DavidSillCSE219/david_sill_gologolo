
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.files;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.Circle_LogoItem;
import gologolo.data.logoitems.GradientOptions;
import gologolo.data.logoitems.Image_LogoItem;
import gologolo.data.logoitems.LogoItem;
import static gologolo.data.logoitems.LogoItemTypes.CIRCLE_ITEM;
import static gologolo.data.logoitems.LogoItemTypes.IMAGE_ITEM;
import static gologolo.data.logoitems.LogoItemTypes.RECTANGLE_ITEM;
import static gologolo.data.logoitems.LogoItemTypes.TEXT_ITEM;
import gologolo.data.logoitems.Rectangle_LogoItem;
import gologolo.data.logoitems.Text_LogoItem;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author doov
 */
public class GoLogoLoFiles implements AppFileComponent {
    
    static final String JSON_NAME               = "name";
    static final String JSON_ITEM_TYPE          = "item_type";
    static final String JSON_X_POS              = "x_Pos";
    static final String JSON_Y_POS              = "y_Pos";
    
    
    static final String JSON_ITEM_ARRAY         = "item_array";
    static final String JSON_ITEM_SIZE          = "item_size";
    static final String JSON_BORDER_THICKNESS   = "border_thickness";
    static final String JSON_BORDER_RADIUS      = "border_radius";
    static final String JSON_BORDER_COLOR       = "border_color";
    static final String JSON_COLOR_R            = "color_r";
    static final String JSON_COLOR_G            = "color_g";
    static final String JSON_COLOR_B            = "color_b";
    static final String JSON_COLOR_O            = "color_o";
    
    static final String JSON_RADIAL_GRADIENT                 = "color_gradient";
    static final String JSON_RADIAL_GRADIENT_FOCUS_ANGLE     = "color_gradient_focus_angle";
    static final String JSON_RADIAL_GRADIENT_FOCUS_DISTANCE  = "color_gradient_focus_distance";
    static final String JSON_RADIAL_GRADIENT_CENTER_X        = "color_gradient_center_x";
    static final String JSON_RADIAL_GRADIENT_CENTER_Y        = "color_gradient_center_y";
    static final String JSON_RADIAL_GRADIENT_RADIUS          = "color_gradient_radius";
    static final String JSON_RADIAL_GRADIENT_CYCLE_METHOD    = "color_gradient_cycle_method";
    static final String JSON_RADIAL_GRADIENT_STOP_0          = "color_gradient_stop_0";
    static final String JSON_RADIAL_GRADIENT_STOP_1          = "color_gradient_stop_1";
    
    
    static final String JSON_TEXT               = "text";
    static final String JSON_TEXT_BOLD          = "is_bold";
    static final String JSON_TEXT_ITALIC        = "is_italic";
    static final String JSON_TEXT_UNDERLINE     = "is_underlined";
    static final String JSON_FONT_FAMILY        = "font_family";
    static final String JSON_FONT_SIZE          = "font_size";
    
    
    static final String JSON_IMAGE_FILE_PATH        = "image_file_path";
    
    
    public GoLogoLoFiles(){
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        GoLogoLoData logoData = (GoLogoLoData)data;
        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        Iterator<Node> logoIt = logoData.getIterator();
        
        while(logoIt.hasNext()){
            Node logoItem = logoIt.next();
            JsonObjectBuilder objBuilder = Json.createObjectBuilder()
                .add(JSON_ITEM_TYPE,    ((LogoItem)logoItem).getItemType().toString())
                .add(JSON_NAME,         ((LogoItem)logoItem).getName())
                .add(JSON_X_POS,        logoItem.getTranslateX())
                .add(JSON_Y_POS,        logoItem.getTranslateY());
            
            if(((LogoItem)logoItem).getItemType().equalsIgnoreCase(RECTANGLE_ITEM.toString())){
                Rectangle_LogoItem rect = ((Rectangle_LogoItem) logoItem);
                objBuilder.add(JSON_BORDER_THICKNESS, rect.getBorderThickness())
                    .add(JSON_BORDER_RADIUS, rect.getBorderRadius())
                    .add(JSON_ITEM_SIZE, rect.getItemSize())
                    .add(JSON_BORDER_COLOR, Json.createObjectBuilder()
                        .add(JSON_COLOR_R, rect.getBorderColor().getRed())
                        .add(JSON_COLOR_G, rect.getBorderColor().getGreen())
                        .add(JSON_COLOR_B, rect.getBorderColor().getBlue())
                        .add(JSON_COLOR_O, rect.getBorderColor().getOpacity())
                    );
                
                
            } else if(((LogoItem)logoItem).getItemType().equalsIgnoreCase(TEXT_ITEM.toString())){
                Text_LogoItem textItem = ((Text_LogoItem)logoItem);
                objBuilder
                    .add(JSON_TEXT,             textItem.getText())
                    .add(JSON_TEXT_BOLD,        textItem.isBold())
                    .add(JSON_TEXT_ITALIC,      textItem.isItalic())
                    .add(JSON_TEXT_UNDERLINE,   textItem.isUnderlined())
                    .add(JSON_FONT_FAMILY,      textItem.getFontFamily())
                    .add(JSON_FONT_SIZE,        textItem.getFontSize());
                
                
            } else if(((LogoItem)logoItem).getItemType().equalsIgnoreCase(IMAGE_ITEM.toString())){
                Image_LogoItem imageItem = ((Image_LogoItem)logoItem);
                objBuilder.add(JSON_IMAGE_FILE_PATH, imageItem.getImageFile().getPath()
                );
                System.out.print(imageItem.getImageFile().exists());
            } else if(((LogoItem)logoItem).getItemType().equalsIgnoreCase(CIRCLE_ITEM.toString())){
                Circle_LogoItem circle = ((Circle_LogoItem) logoItem);
                objBuilder.add(JSON_BORDER_THICKNESS, circle.getBorderThickness())
                    .add(JSON_ITEM_SIZE, circle.getItemSize())
                    .add(JSON_BORDER_COLOR, Json.createObjectBuilder()
                        .add(JSON_COLOR_R, circle.getBorderColor().getRed())
                        .add(JSON_COLOR_G, circle.getBorderColor().getGreen())
                        .add(JSON_COLOR_B, circle.getBorderColor().getBlue())
                        .add(JSON_COLOR_O, circle.getBorderColor().getOpacity())
                    );
            }
            
            if(((LogoItem)logoItem).hasColorGradientOptions()){
                RadialGradient gradient = ((GradientOptions)logoItem).getRadialGradient();
                Color color0 = gradient.getStops().get(0).getColor();
                Color color1 = gradient.getStops().get(1).getColor();
                
                objBuilder
                    .add(JSON_RADIAL_GRADIENT, Json.createObjectBuilder()
                        .add(JSON_RADIAL_GRADIENT_FOCUS_ANGLE,      gradient.getFocusAngle())
                        .add(JSON_RADIAL_GRADIENT_FOCUS_DISTANCE,   gradient.getFocusDistance())
                        .add(JSON_RADIAL_GRADIENT_CENTER_X,         gradient.getCenterX())
                        .add(JSON_RADIAL_GRADIENT_CENTER_Y,         gradient.getCenterY())
                        .add(JSON_RADIAL_GRADIENT_RADIUS,           gradient.getCenterY())
                        .add(JSON_RADIAL_GRADIENT_CYCLE_METHOD,     gradient.getCycleMethod().toString())
                        .add(JSON_RADIAL_GRADIENT_STOP_0,           Json.createObjectBuilder()
                                .add(JSON_COLOR_R, color0.getRed())
                                .add(JSON_COLOR_G, color0.getGreen())
                                .add(JSON_COLOR_B, color0.getBlue())
                                .add(JSON_COLOR_O, color0.getOpacity()))
                        .add(JSON_RADIAL_GRADIENT_STOP_1,           Json.createObjectBuilder()
                                .add(JSON_COLOR_R, color1.getRed())
                                .add(JSON_COLOR_G, color1.getGreen())
                                .add(JSON_COLOR_B, color1.getBlue())
                                .add(JSON_COLOR_O, color1.getOpacity()))
                        
                );
            }
            
            
            JsonObject jsonObject = objBuilder.build();

            
            arrayBuilder.add(jsonObject);
        }
        JsonArray itemsArray = arrayBuilder.build();
        
        JsonObject logoJson = Json.createObjectBuilder()
                .add(JSON_ITEM_ARRAY, itemsArray).build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(logoJson);
	jsonWriter.close();

	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(logoJson);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    //double borderThickness, double borderRadius, Color borderColor, RadialGradient colorGradient, String name

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        GoLogoLoData logoData = (GoLogoLoData)data;
        logoData.reset();
        
        JsonObject json = loadJSONFile(filePath);
        
        JsonArray jsonArray = json.getJsonArray(JSON_ITEM_ARRAY);
        
        for(int i = 0; i < jsonArray.size(); i++){
            JsonObject jsonItem = jsonArray.getJsonObject(i);
            String itemType = jsonItem.getString(JSON_ITEM_TYPE);
            if(itemType.equalsIgnoreCase(RECTANGLE_ITEM.toString())){
                logoData.addItem(loadRectangle(jsonItem));
            } else if (itemType.equalsIgnoreCase(TEXT_ITEM.toString())){
                logoData.addItem(loadText(jsonItem));
            } else if (itemType.equalsIgnoreCase(IMAGE_ITEM.toString())){
                logoData.addItem(loadJsonImage(jsonItem));
            } else if (itemType.equalsIgnoreCase(CIRCLE_ITEM.toString())){
                logoData.addItem(loadJsonCircle(jsonItem));
            }
        }
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        GoLogoLoData logoData = (GoLogoLoData)data;
        WritableImage image = logoData.getWorkPane().snapshot(new SnapshotParameters(), null);
        FileChooser fc = new FileChooser();
        File file = new File(filePath);
        try{
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ex) {
            System.err.print(ex.getMessage());
        }
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Node loadRectangle(JsonObject jsonRect) {
        double borderThickness = jsonRect.getJsonNumber(JSON_BORDER_THICKNESS).doubleValue();
        double borderRadius = jsonRect.getJsonNumber(JSON_BORDER_RADIUS).doubleValue();
        Color borderColor = loadColor(jsonRect.getJsonObject(JSON_BORDER_COLOR));
        String name = jsonRect.getString(JSON_NAME);
        double xPos = jsonRect.getJsonNumber(JSON_X_POS).doubleValue();
        double yPos = jsonRect.getJsonNumber(JSON_Y_POS).doubleValue();
        double size = jsonRect.getJsonNumber(JSON_ITEM_SIZE).doubleValue();
        RadialGradient gradient = loadRadialGradient(jsonRect.getJsonObject(JSON_RADIAL_GRADIENT));
        
        Rectangle_LogoItem  rect = new Rectangle_LogoItem(borderThickness, borderRadius, borderColor, null, name);
        rect.setTranslateX(xPos);
        rect.setTranslateY(yPos);
        rect.setItemSize(size);
        rect.setRadialGradient(gradient);
        return rect;
        
    }

    private Node loadText(JsonObject jsonText) {
        String text =           jsonText.getString(JSON_TEXT);
        String name =           jsonText.getString(JSON_NAME);
        double xPos =           jsonText.getJsonNumber(JSON_X_POS).doubleValue();
        double yPos =           jsonText.getJsonNumber(JSON_Y_POS).doubleValue();
        boolean isBold =        jsonText.getBoolean(JSON_TEXT_BOLD);
        boolean isItalic =      jsonText.getBoolean(JSON_TEXT_ITALIC);
        boolean isunderLined =  jsonText.getBoolean(JSON_TEXT_UNDERLINE);
        String fontFamily =     jsonText.getString(JSON_FONT_FAMILY);
        int fontSize =          jsonText.getInt(JSON_FONT_SIZE);
        
        Text_LogoItem textItem = new Text_LogoItem(text);
        textItem.setName(name);
        textItem.setTranslateX(xPos);
        textItem.setTranslateY(yPos);
        textItem.setFontFamily(fontFamily);
        textItem.setFontSize(fontSize);
        if(isBold){
            textItem.toggleBold();
        }
        if(isItalic){
            textItem.toggleItalic();
        }
        if(isunderLined){
            textItem.toggleUnderline();
        }
        return textItem;
    }
    
    private RadialGradient loadRadialGradient(JsonObject jsonGradient){
        double focusAngle       = jsonGradient.getJsonNumber(JSON_RADIAL_GRADIENT_FOCUS_ANGLE).doubleValue();
        double focusDistance    = jsonGradient.getJsonNumber(JSON_RADIAL_GRADIENT_FOCUS_DISTANCE).doubleValue();
        double centerX          = jsonGradient.getJsonNumber(JSON_RADIAL_GRADIENT_CENTER_X).doubleValue();     
        double centerY          = jsonGradient.getJsonNumber(JSON_RADIAL_GRADIENT_CENTER_Y).doubleValue();
        double radius           = jsonGradient.getJsonNumber(JSON_RADIAL_GRADIENT_RADIUS).doubleValue();
        CycleMethod cycle       = CycleMethod.valueOf(jsonGradient.getJsonString(JSON_RADIAL_GRADIENT_CYCLE_METHOD).getString());
        Color stop0             = loadColor(jsonGradient.getJsonObject(JSON_RADIAL_GRADIENT_STOP_0));
        Color stop1             = loadColor(jsonGradient.getJsonObject(JSON_RADIAL_GRADIENT_STOP_1));
        Stop[] stops = {new Stop(0, stop0), new Stop(1, stop1)};
        return new RadialGradient(focusAngle, focusDistance, centerX, centerY, radius, true, cycle, stops);
    }

    private Color loadColor(JsonObject jsonColor) {
        double red = jsonColor.getJsonNumber(JSON_COLOR_R).doubleValue();
        double green = jsonColor.getJsonNumber(JSON_COLOR_G).doubleValue();
        double blue = jsonColor.getJsonNumber(JSON_COLOR_B).doubleValue();
        double opacity = jsonColor.getJsonNumber(JSON_COLOR_O).doubleValue();
        return new Color(red, green, blue, opacity);
    }
    
    public File selectImage(GoLogoLoApp app){
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image files", "*.jpg", "*.png"));
        return fc.showOpenDialog(app.getGUIModule().getWindow());
    }
    
    public Image loadImageFile(File file){
        Image image = null;
        try {
            FileInputStream input = new FileInputStream(file);
            image = new Image(input);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GoLogoLoFiles.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }

    private Node loadJsonImage(JsonObject jsonImage) {
        String name = jsonImage.getString(JSON_NAME);
        double xPos = jsonImage.getJsonNumber(JSON_X_POS).doubleValue();
        double yPos = jsonImage.getJsonNumber(JSON_Y_POS).doubleValue();
        String path = jsonImage.get(JSON_IMAGE_FILE_PATH).toString();
        path = path.substring(1, path.length() -1);
        File file = new File(path);
        
        Image image = loadImageFile(file);
        Image_LogoItem imageItem = new Image_LogoItem(image, file);
        imageItem.setName(name);
        imageItem.setTranslateX(xPos);
        imageItem.setTranslateY(yPos);
        return imageItem;
    }

    private Node loadJsonCircle(JsonObject jsonCircle) {
        double borderThickness = jsonCircle.getJsonNumber(JSON_BORDER_THICKNESS).doubleValue();
        Color borderColor = loadColor(jsonCircle.getJsonObject(JSON_BORDER_COLOR));
        String name = jsonCircle.getString(JSON_NAME);
        double xPos = jsonCircle.getJsonNumber(JSON_X_POS).doubleValue();
        double yPos = jsonCircle.getJsonNumber(JSON_Y_POS).doubleValue();
        double size = jsonCircle.getJsonNumber(JSON_ITEM_SIZE).doubleValue();
        RadialGradient gradient = loadRadialGradient(jsonCircle.getJsonObject(JSON_RADIAL_GRADIENT));
        
        Circle_LogoItem circle = new Circle_LogoItem(borderThickness, borderColor, name);
        circle.setTranslateX(xPos);
        circle.setTranslateY(yPos);
        circle.setItemSize(size);
        circle.setRadialGradient(gradient);
        return circle;
        
    }
    
}
