/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.LogoItem;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeBorderThickness_Transaction implements jTPS_Transaction {
    double oldThickness;
    double newThickness;
    Node item;
    GoLogoLoData data;

    public ChangeBorderThickness_Transaction(double newThickness, Node item, GoLogoLoData data) {
        this.newThickness = newThickness;
        this.item = item;
        this.data = data;
        this.oldThickness = ((LogoItem)item).getBorderRadius();
    }
    
    @Override
    public void doTransaction() {
        ((LogoItem)item).setBorderThickness(newThickness);
    }

    @Override
    public void undoTransaction() {
        ((LogoItem)item).setBorderThickness(oldThickness);
    }
    
}
