/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class MoveLogoItem_Transaction  implements jTPS_Transaction{
    double initialX;
    double initialY;
    double targetX;
    double targetY;
    Node item;

    public MoveLogoItem_Transaction(double initialX, double initialY, Node item) {
        this.initialX = initialX;
        this.initialY = initialY;
        this.item = item;
        this.targetX = item.getTranslateX();
        this.targetY = item.getTranslateY();
    }

    @Override
    public void doTransaction() {
        this.item.setTranslateX(targetX);
        this.item.setTranslateY(targetY);
    }

    @Override
    public void undoTransaction() {
        this.item.setTranslateX(initialX);
        this.item.setTranslateY(initialY);
    }
    
    
    
}
