/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.LogoItem;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeItemName_Transaction implements jTPS_Transaction{
    String oldName;
    String newName;
    Node item;

    public ChangeItemName_Transaction(String newName, Node item) {
        this.newName = newName;
        this.item = item;
        this.oldName = ((LogoItem)item).getName();
    }
    
    

    @Override
    public void doTransaction() {
        ((LogoItem)item).setName(newName);
    }

    @Override
    public void undoTransaction() {
        ((LogoItem)item).setName(oldName);
    }
    
}
