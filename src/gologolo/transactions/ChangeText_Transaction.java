/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.Text_LogoItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeText_Transaction implements jTPS_Transaction{

    String newText;
    String oldText;
    Text_LogoItem textItem;

    public ChangeText_Transaction(String newText, Text_LogoItem textItem) {
        this.newText = newText;
        this.oldText = textItem.getText();
        this.textItem = textItem;
    }
    
    @Override
    public void doTransaction() {
        textItem.setText(newText);
    }

    @Override
    public void undoTransaction() {
        textItem.setText(oldText);
    }
    
}
