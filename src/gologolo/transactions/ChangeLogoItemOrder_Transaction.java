/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeLogoItemOrder_Transaction implements jTPS_Transaction {
    int oldIndex;
    int newIdex;
    Node item;
    GoLogoLoData data;

    public ChangeLogoItemOrder_Transaction(int newIdex, Node item, GoLogoLoData data) {
        this.newIdex = newIdex;
        this.item = item;
        this.data = data;
        this.oldIndex = data.getLogoItems().indexOf(item);
    }
    
    
    @Override
    public void doTransaction() {
        data.moveItem(newIdex, item);
    }

    @Override
    public void undoTransaction() {
        data.moveItem(oldIndex, item);
    }
    
}
