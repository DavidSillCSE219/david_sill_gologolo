/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class DeleteLogoItem_Transaction implements jTPS_Transaction {

    GoLogoLoData data;
    Node itemToDelete;
    
    public DeleteLogoItem_Transaction(GoLogoLoData data, Node itemToDelete) {
    this.data = data;
    this.itemToDelete = itemToDelete;
    }
    
    @Override
    public void doTransaction() {
        data.removeItem(itemToDelete);
    }

    @Override
    public void undoTransaction() {
        data.addItem(itemToDelete);
    }
    
}
