/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class AddLogoItem_Transaction implements jTPS_Transaction{
    
    GoLogoLoData data;
    Node itemToAdd;

    public AddLogoItem_Transaction(GoLogoLoData data, Node itemToAdd) {
        this.data = data;
        this.itemToAdd = itemToAdd;
    }
    
    @Override
    public void doTransaction() {
        data.addItem(itemToAdd);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
    }
    
}
