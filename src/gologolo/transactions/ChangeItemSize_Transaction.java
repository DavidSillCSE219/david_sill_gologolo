/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.LogoItem;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeItemSize_Transaction implements jTPS_Transaction{
    double newSize;
    double oldSize;
    Node item;
    
    
    public ChangeItemSize_Transaction(double newSize, Node item){
        this.item = item;
        this.newSize = newSize;
        this.oldSize = ((LogoItem)item).getItemSize();
    }

    @Override
    public void doTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void undoTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
