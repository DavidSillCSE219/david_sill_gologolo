/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.LogoItem;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeBorderColor_Transaction implements jTPS_Transaction {
    Color oldColor;
    Color newColor;
    Node item;
    GoLogoLoData data;

    public ChangeBorderColor_Transaction(Color newColor, Node item, GoLogoLoData data) {
        this.newColor = newColor;
        this.item = item;
        this.data = data;
        this.oldColor = ((LogoItem)item).getBorderColor();
    }   

    @Override
    public void doTransaction() {
        ((LogoItem)item).setBorderColor(newColor);
    }

    @Override
    public void undoTransaction() {
        ((LogoItem)item).setBorderColor(oldColor);
    }
    
}
