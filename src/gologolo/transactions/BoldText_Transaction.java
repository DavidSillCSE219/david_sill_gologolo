/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.Text_LogoItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class BoldText_Transaction implements jTPS_Transaction {
    Text_LogoItem item;
    
    public BoldText_Transaction(Text_LogoItem item) {
        this.item = item;
    }

    @Override
    public void doTransaction() {
        item.toggleBold();
    }

    @Override
    public void undoTransaction() {
        item.toggleBold();
    }
    
}
