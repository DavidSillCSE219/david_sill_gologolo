/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.Text_LogoItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangFontFamily_Transaction implements jTPS_Transaction{
    String oldFont;
    String newFont;
    Text_LogoItem item;

    public ChangFontFamily_Transaction(String newFont, Text_LogoItem item) {
        this.newFont = newFont;
        this.item = item;
        this.oldFont = item.getFontFamily();
    }

    @Override
    public void doTransaction() {
        item.setFontFamily(newFont);
    }

    @Override
    public void undoTransaction() {
        item.setFontFamily(oldFont);
    }
    
}
