/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.LogoItem;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeBorderRadius_Transaction implements jTPS_Transaction {
    double oldRadius;
    double newRadius;
    Node item;
    GoLogoLoData data;

    public ChangeBorderRadius_Transaction(double newRadius, Node item) {
        this.newRadius = newRadius;
        this.item = item;
        this.data = data;
        this.oldRadius = ((LogoItem)item).getBorderRadius();
    }
    
    @Override
    public void doTransaction() {
        ((LogoItem)item).setBorderRadius(newRadius);
    }

    @Override
    public void undoTransaction() {
        ((LogoItem)item).setBorderRadius(oldRadius);
    }
    
}
