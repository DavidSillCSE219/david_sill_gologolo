/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.logoitems.Text_LogoItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author doov
 */
public class ChangeFontSize_Transaction implements jTPS_Transaction{

    Text_LogoItem item;
    int newFontSize;
    int oldFontSize;

    public ChangeFontSize_Transaction(Text_LogoItem item, int newFontSize) {
        this.item = item;
        this.newFontSize = newFontSize;
        this.oldFontSize = item.getFontSize();
    }
    
    
    
    @Override
    public void doTransaction() {
        item.setFontSize(newFontSize);
    }

    @Override
    public void undoTransaction() {
        item.setFontSize(oldFontSize);
    }
    
    
}
