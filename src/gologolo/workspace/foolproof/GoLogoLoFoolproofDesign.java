/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_CONTROL_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_RADIUS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_THICKNESS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SELECT_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SIZE_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_SIZE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_X_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_Y_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CONTROL_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_RADIUS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_0_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_1_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_TABLE_CONTROLS_PANE;
import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.GradientOptions;
import gologolo.data.logoitems.LogoItem;
import gologolo.data.logoitems.LogoItemTypes;
import gologolo.data.logoitems.Text_LogoItem;
import gologolo.workspace.GoLogoLoWorkspace;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.paint.RadialGradient;

/**
 *
 * @author doov
 */
public class GoLogoLoFoolproofDesign implements FoolproofDesign{
    GoLogoLoApp app;

    public GoLogoLoFoolproofDesign(GoLogoLoApp app) {
        this.app = app;
    }
    
    

    @Override
    public void updateControls() {
        GoLogoLoWorkspace workspace = ((GoLogoLoWorkspace)app.getWorkspaceComponent());
        workspace.settingValue = true;
        AppGUIModule gui = app.getGUIModule();
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        LogoItem selected = ((LogoItem)data.getSelectedItem());
        
        gui.getGUINode(LOGO_TABLE_CONTROLS_PANE).setDisable(!(data.isItemSelected()));
        
        
        gui.getGUINode(LOGO_FONT_PANE).setDisable(!(data.isItemSelected() && selected.hasTextOptions()));
        if(data.isItemSelected() && selected.hasTextOptions()){
            ((ComboBox)gui.getGUINode(LOGO_FONT_SELECT_COMBO_BOX))  .setValue(((Text_LogoItem)selected).getFontFamily());
            ((ComboBox)gui.getGUINode(LOGO_FONT_SIZE_COMBO_BOX))    .setValue(""+((Text_LogoItem)selected).getFontSize());
        }
        
        gui.getGUINode(LOGO_BORDER_CONTROL_PANE).setDisable(!(data.isItemSelected() && selected.hasBorderOptions()));
        if(data.isItemSelected() && selected.hasBorderOptions()){
            ((Slider)gui.getGUINode(LOGO_BORDER_THICKNESS_SLIDER))      .setValue(selected.getBorderThickness());
            ((ColorPicker)gui.getGUINode(LOGO_BORDER_COLOR_PICKER))     .setValue(selected.getBorderColor());
            ((Slider)gui.getGUINode(LOGO_BORDER_RADIUS_SLIDER))         .setValue(selected.getBorderRadius());
            ((Slider)gui.getGUINode(LOGO_ITEM_SIZE_SLIDER))             .setValue(selected.getItemSize());
            if(((LogoItem)data.getSelectedItem()).getItemType().equalsIgnoreCase(LogoItemTypes.CIRCLE_ITEM.toString())){
                ((Slider)gui.getGUINode(LOGO_BORDER_RADIUS_SLIDER)).setDisable(true);
            }
        }
        
        
        
        
        gui.getGUINode(LOGO_RADIAL_GRADIENT_CONTROL_PANE).setDisable(!(data.isItemSelected() && selected.hasColorGradientOptions()));
        if(data.isItemSelected() && selected.hasColorGradientOptions()){
            RadialGradient gradient = ((GradientOptions)selected).getRadialGradient();
            ((Slider)gui.getGUINode(LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_SLIDER))       .setValue(gradient.getFocusAngle());
            ((Slider)gui.getGUINode(LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_SLIDER))    .setValue(gradient.getFocusDistance());
            ((Slider)gui.getGUINode(LOGO_RADIAL_GRADIENT_CENTER_X_SLIDER))          .setValue(gradient.getCenterX());
            ((Slider)gui.getGUINode(LOGO_RADIAL_GRADIENT_CENTER_Y_SLIDER))          .setValue(gradient.getCenterY());
            ((Slider)gui.getGUINode(LOGO_RADIAL_GRADIENT_RADIUS_SLIDER))            .setValue(gradient.getRadius());
            ((ComboBox)gui.getGUINode(LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX)) .setValue(gradient.getCycleMethod().toString());
            ((ColorPicker)gui.getGUINode(LOGO_RADIAL_GRADIENT_STOP_0_COLOR_PICKER)) .setValue(gradient.getStops().get(0).getColor());
            ((ColorPicker)gui.getGUINode(LOGO_RADIAL_GRADIENT_STOP_1_COLOR_PICKER)) .setValue(gradient.getStops().get(1).getColor());
            ((GoLogoLoWorkspace)app.getWorkspaceComponent()).getGradientController().setGradient(gradient);
        }
        workspace.settingValue = false;
    }
    
}
