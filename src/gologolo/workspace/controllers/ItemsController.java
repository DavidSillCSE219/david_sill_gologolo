/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.Circle_LogoItem;
import gologolo.data.logoitems.GradientOptions;
import gologolo.data.logoitems.Image_LogoItem;
import gologolo.data.logoitems.LogoItem;
import gologolo.data.logoitems.Rectangle_LogoItem;
import gologolo.data.logoitems.Text_LogoItem;
import gologolo.files.GoLogoLoFiles;
import gologolo.transactions.AddLogoItem_Transaction;
import gologolo.transactions.BoldText_Transaction;
import gologolo.transactions.ChangFontFamily_Transaction;
import gologolo.transactions.ChangeBorderColor_Transaction;
import gologolo.transactions.ChangeBorderRadius_Transaction;
import gologolo.transactions.ChangeBorderThickness_Transaction;
import gologolo.transactions.ChangeFontSize_Transaction;
import gologolo.transactions.ChangeItemName_Transaction;
import gologolo.transactions.ChangeItemSize_Transaction;
import gologolo.transactions.ChangeLogoItemOrder_Transaction;
import gologolo.transactions.ChangeText_Transaction;
import gologolo.transactions.DeleteLogoItem_Transaction;
import gologolo.transactions.ItalicText_Transaction;
import gologolo.workspace.dialogs.ChangeItemNameDialog;
import gologolo.workspace.dialogs.EditTextDialog;
import java.io.File;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;

/**
 *
 * @author doov
 */
public class ItemsController {

    GoLogoLoApp app;
    EditTextDialog editTextDialog;
    ChangeItemNameDialog changeNameDialog;
    DragController dragController;

    public ItemsController(GoLogoLoApp app) {
        this.app = app;
        editTextDialog = new EditTextDialog(app);
        dragController = new DragController(app);
        changeNameDialog = new ChangeItemNameDialog(app);
    }
    
    public void addText(){
        editTextDialog.showCreateTextDialog();
        String text = editTextDialog.getInputText();
        if(text != null){
            Text_LogoItem textItem = new Text_LogoItem(text);
            addText(textItem);
        }
        
    }
    
    public void addText(Text_LogoItem textItem){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddLogoItem_Transaction transaction = new AddLogoItem_Transaction(data, textItem);
        app.processTransaction(transaction);
        textItem.setOnMouseClicked(e -> {
            data.setSelectedItem(textItem);
            if(e.getClickCount() >= 2){
                editTextDialog.showEditTextDialog(textItem);
                String newText = editTextDialog.getInputText().trim();
                if(newText != null){
                    ChangeText_Transaction changeTextTransaction = new ChangeText_Transaction(newText, textItem);
                    app.processTransaction(changeTextTransaction);
                }
            }
            e.consume();
            });
    }
    public void addRectangle(){
        addRectangle(new Rectangle_LogoItem());
    }
    
    public void addRectangle(Rectangle_LogoItem rect){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddLogoItem_Transaction transaction = new AddLogoItem_Transaction(data, rect);
        app.processTransaction(transaction);
        data.setSelectedItem(rect);
    }
    public void addCircle(){
        addCircle(new Circle_LogoItem());
    }
    public void addCircle(Circle_LogoItem circle){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddLogoItem_Transaction transaction = new AddLogoItem_Transaction(data, circle);
        app.processTransaction(transaction);
        data.setSelectedItem(circle);
    }
    
    public void addImage(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        GoLogoLoFiles fileComp = (GoLogoLoFiles)app.getFileComponent();
        File file = fileComp.selectImage(app);
        Image image = fileComp.loadImageFile(file);
        Image_LogoItem imageItem = new Image_LogoItem(image, file);
        addImage(imageItem);
    }
    public void addImage(Image_LogoItem imageItem){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddLogoItem_Transaction transaction = new AddLogoItem_Transaction(data, imageItem);
        app.processTransaction(transaction);
        data.setSelectedItem(imageItem);
    }
    
    public void clearSelected(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        data.clearSelected();
        
        
    }

    public DragController getDragController() {
        return dragController;
    }

    public void deleteSelected() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.getSelectedItem() != null){
            DeleteLogoItem_Transaction transaction = new DeleteLogoItem_Transaction(data, data.getSelectedItem());
            app.processTransaction(transaction);
        }
    }
    
    public void changeItemName(){
         GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
         if(data.isItemSelected()){
             LogoItem item = ((LogoItem) data.getSelectedItem());
             System.out.println(item.getName());
             changeNameDialog.showChangeNameDialog(item.getName());
             if(changeNameDialog.getNewName() != null){
                 ChangeItemName_Transaction transaction = new ChangeItemName_Transaction(changeNameDialog.getNewName(), (Node)item);
                 app.processTransaction(transaction);
             }
         }
    }
    
    public void moveSelectedItemUp(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        ChangeLogoItemOrder_Transaction transaction = new ChangeLogoItemOrder_Transaction(data.getLogoItems().indexOf(item) + 1, item, data);
        app.processTransaction(transaction);
    }
    
    public void moveSelectedItemDown(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        ChangeLogoItemOrder_Transaction transaction = new ChangeLogoItemOrder_Transaction(data.getLogoItems().indexOf(item) - 1, item, data);
        app.processTransaction(transaction);
    }

    public void setItemBorderColor(Color value) {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        ChangeBorderColor_Transaction transaction = new ChangeBorderColor_Transaction(value, item, data);
        app.processTransaction(transaction);
    }
    
    public void setItemBorderRadius(double value, boolean setValue){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        if(setValue) {
            ChangeBorderRadius_Transaction transaction = new ChangeBorderRadius_Transaction(value, item);
            app.processTransaction(transaction);
        } else {
            ((LogoItem)item).setBorderRadius(value);
        } 
    }
    public void setItemBorderThickness(double value, boolean setValue){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        if(setValue) {
            ChangeBorderThickness_Transaction transaction = new ChangeBorderThickness_Transaction(value, item, data);
            app.processTransaction(transaction);
        } else {
            ((LogoItem)item).setBorderThickness(value);
        } 
    }
    
    public void toggleSelectedBold(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
        BoldText_Transaction transaction = new BoldText_Transaction(item);
        app.processTransaction(transaction);
    }
    
    public void toggleSelectedItalic(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
        ItalicText_Transaction transaction = new ItalicText_Transaction(item);
        app.processTransaction(transaction);
    }

    public void decreaseSelectedFontSize() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
        if(item.getFontSize() -2 > 0){
            ChangeFontSize_Transaction transaction = new ChangeFontSize_Transaction(item, item.getFontSize() -2);
            app.processTransaction(transaction);
        }
        
    }

    public void increaseSelectedFontSize() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
        ChangeFontSize_Transaction transaction = new ChangeFontSize_Transaction(item, item.getFontSize() + 2);
        app.processTransaction(transaction);
        
    }
    
    public void toggleGridSnap(boolean snap){
        dragController.setSnap(snap);
    }

    public void changeItemSize(double value, boolean setValue) {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        Node item = data.getSelectedItem();
        if(setValue) {
            ChangeItemSize_Transaction transaction = new ChangeItemSize_Transaction(value, item);
            app.processTransaction(transaction);
        } else {
            ((LogoItem)item).setItemSize(value);
        } 
    }

    public void updateGradient(RadialGradient gradient) {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected()){
            Node item = data.getSelectedItem();
            ((GradientOptions)item).setRadialGradient(gradient);
        }
    }

    public void underLineText() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected()){
            Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
            item.toggleUnderline();
        }
    }
    
    public void setFont(String family){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected()){
            Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
            ChangFontFamily_Transaction transaction = new ChangFontFamily_Transaction(family, item);
            app.processTransaction(transaction);
        }
    }
    
    public void setFontSize(int size){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected()){
            Text_LogoItem item = (Text_LogoItem)data.getSelectedItem();
            ChangeFontSize_Transaction transaction = new ChangeFontSize_Transaction(item, size);
            app.processTransaction(transaction);
        }
    }
}
