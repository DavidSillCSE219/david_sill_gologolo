/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author doov
 */
public class RadialGradientController {
    
    private RadialGradient gradient;
    private double focusAngle;
    private double focusDistance;
    private double centerX;
    private double centerY;
    private double radius;
    final private boolean proportional = true;
    private CycleMethod cycleMethod;
    private Stop[] stops;
    
    public RadialGradientController(){
        stops = new Stop[2];
        stops[0] = new Stop(0.0, Color.BLUE);
        stops[1] = new Stop(0.0, Color.RED);
        this.focusAngle = 1.0;
        this.focusDistance = 1.0;
        this.centerX = 1.0;
        this.centerY = 1.0;
        this.radius = 1.0;
        cycleMethod = CycleMethod.NO_CYCLE;
        gradient = new RadialGradient(focusAngle, focusDistance, centerX, centerY, radius, proportional, cycleMethod, stops);
        
    }


    public RadialGradient getGradient() {
        return gradient;
    }

    public double getFocusAngle() {
        return focusAngle;
    }

    public double getFocusDistance() {
        return focusDistance;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getRadius() {
        return radius;
    }

    public CycleMethod getCycleMethod() {
        return cycleMethod;
    }

    public Stop[] getStops() {
        return stops;
    }

    public void setGradient(RadialGradient gradient) {
        this.gradient = gradient;
        this.centerX = gradient.getCenterX();
        this.centerY = gradient.getCenterY();
        this.focusAngle = gradient.getFocusAngle();
        this.focusDistance = gradient.getFocusDistance();
        this.radius = gradient.getRadius();
        this.cycleMethod = gradient.getCycleMethod();
        this.stops[0] = gradient.getStops().get(0);
        this.stops[1] = gradient.getStops().get(1);
    }

    public void setFocusAngle(double focusAngle) {
        this.focusAngle = focusAngle;
        upDateGradient();
        
    }

    public void setFocusDistance(double focusDistance) {
        this.focusDistance = focusDistance;
        upDateGradient();
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
        upDateGradient();
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
        upDateGradient();
    }

    public void setRadius(double radius) {
        this.radius = radius;
        upDateGradient();
    }

    public void setCycleMethod(CycleMethod cycleMethod) {
        this.cycleMethod = cycleMethod;
        upDateGradient();
    }

    public void setStops(Stop[] stops) {
        this.stops = stops;
        upDateGradient();
    }
    
    public void setStop0(Color color){
        stops[0] = new Stop(0, color);
        upDateGradient();
    }
    public void setStop1(Color color){
        stops[1] = new Stop(1, color);
        upDateGradient();
    }
    
    private void upDateGradient(){
        this.gradient = new RadialGradient(focusAngle, focusDistance, centerX, centerY, radius, proportional, cycleMethod, stops[0], stops[1]);
    }
    
    
    
    
    
    
    
}
