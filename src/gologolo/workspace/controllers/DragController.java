/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controllers;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.transactions.MoveLogoItem_Transaction;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author doov
 */
public class DragController {
    
    class DragContext{
        double x;
        double y;
    }
    
    private DragContext dragContext = new DragContext();
    private DragContext initialPos = new DragContext();
    private GoLogoLoApp app;
    private GoLogoLoData data;
    private boolean snap;

    public DragController(GoLogoLoApp app) {
        snap = false;
        this.app = app;
    }

    public boolean isSnap() {
        return snap;
    }

    public void setSnap(boolean snap) {
        this.snap = snap;
    }
    
    
    
    public void makeDraggable(Node item){
        if(data == null){
            data = (GoLogoLoData)app.getDataComponent();
        }
        item.setOnMouseClicked(onMouseClickEventHandler);
        item.setOnMousePressed(onMousePressedEventHandler);
        item.setOnMouseDragged(onMouseDraggedEventHandler);
        item.setOnMouseReleased(OnMouseDragReleasedEventHandler);
    }
    
    public void makeMiddleMouseDraggable(Node item){
        item.setOnMousePressed(onMiddleMousePressedEventHandler);
        item.setOnMouseDragged(onMiddleMouseDraggedEventHandler);
//        item.setOnMouseReleased(OnMouseDragReleasedEventHandler);
    }
    
    EventHandler<MouseEvent> onMousePressedEventHandler = e -> {
        Node item = ((Node)e.getSource());
        if(e.getButton() == MouseButton.PRIMARY){
            initialPos.x = item.getTranslateX();
            initialPos.y = item.getTranslateY();

            dragContext.x = initialPos.x - e.getSceneX();
            dragContext.y = initialPos.y - e.getSceneY();
        }
    };
    
            
            
    EventHandler<MouseEvent> onMouseDraggedEventHandler = e -> {
        Node item = ((Node)e.getSource());
        if(e.getButton() == MouseButton.PRIMARY){
            double xPos = dragContext.x + e.getSceneX();
            double yPos = dragContext.y + e.getSceneY();
            if (snap) {
                xPos -= (xPos % 10);
                yPos -= (yPos % 10);
            }

            item.setTranslateX(xPos);
            item.setTranslateY(yPos);
        }
        
    };
    
    EventHandler<Event> OnMouseDragReleasedEventHandler = e -> {
        Node item = ((Node)e.getSource());
        if(initialPos.x != item.getTranslateX() || initialPos.y != item.getTranslateY()){
            
            MoveLogoItem_Transaction transaction = new MoveLogoItem_Transaction(initialPos.x, initialPos.y, item);
            app.processTransaction(transaction);
            
        }
    };
    
    EventHandler<MouseEvent> onMouseClickEventHandler = e -> {
        Node item = ((Node)e.getSource());
        data.setSelectedItem(item);
        e.consume();
    };
    
    EventHandler<MouseEvent> onMiddleMouseDraggedEventHandler = e -> {
        Node item = ((Node)e.getSource());
        if(e.getButton() == MouseButton.MIDDLE){
            double xPos = dragContext.x + e.getSceneX();
            double yPos = dragContext.y + e.getSceneY();

            item.setTranslateX(xPos);
            item.setTranslateY(yPos); 
        }
    };
    
    EventHandler<MouseEvent> onMiddleMousePressedEventHandler = e -> {
        Node item = ((Node) e.getSource());
        if(e.getButton() == MouseButton.MIDDLE){
            initialPos.x = item.getTranslateX();
            initialPos.y = item.getTranslateY();

            dragContext.x = initialPos.x - e.getSceneX();
            dragContext.y = initialPos.y - e.getSceneY();
        }
        
    };
    
    
}
