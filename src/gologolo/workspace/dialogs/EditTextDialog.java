/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;

import djf.modules.AppLanguageModule;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.EDIT_TEXT_DIALOG_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.EDIT_TEXT_DIALOG_LABEL;
import static gologolo.GoLogoLoPropertyType.EDIT_TEXT_DIALOG_OKAY_BUTTON;
import gologolo.data.logoitems.Text_LogoItem;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_BUTTON;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_GRID;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT_FIELD;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author doov
 */
public class EditTextDialog extends Stage {

    GoLogoLoApp app;
    GridPane grid;
    TextField textDialogField;
    String inputText;
    
    
    public EditTextDialog(GoLogoLoApp app) {
        this.app = app;

        initDialog();
        
        Scene scene = new Scene(grid);
        this.setScene(scene);
                                  
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_LOGO_DIALOG_GRID);
        
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    public void showCreateTextDialog(){
        inputText = null;
        showAndWait();
    }
    
    public void showEditTextDialog(Text_LogoItem textItem){
        inputText = null;
        textDialogField.setText(textItem.getText());
        showAndWait();
    }

    private void initDialog() {
        grid = new GridPane();
            grid.getStyleClass().add(CLASS_LOGO_DIALOG_GRID);
        
        AppLanguageModule languageSettings = app.getLanguageModule();
        Label textDialogLabel = new Label();
            textDialogLabel.getStyleClass().add(CLASS_LOGO_TEXT);
            languageSettings.addLabeledControlProperty(EDIT_TEXT_DIALOG_LABEL + "_TEXT", ((Labeled)textDialogLabel).textProperty());
        textDialogField = new TextField();
            textDialogField.getStyleClass().add(CLASS_LOGO_TEXT_FIELD);
            
        Button okButton = new Button();
            languageSettings.addLabeledControlProperty(EDIT_TEXT_DIALOG_OKAY_BUTTON + "_TEXT", ((Labeled)okButton).textProperty());
            okButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(EDIT_TEXT_DIALOG_OKAY_BUTTON, okButton);
            okButton.setOnAction(e -> {
                processOkay();
                });
        Button cancelButton = new Button();
            languageSettings.addLabeledControlProperty(EDIT_TEXT_DIALOG_CANCEL_BUTTON + "_TEXT", ((Labeled)cancelButton).textProperty());
            cancelButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(EDIT_TEXT_DIALOG_CANCEL_BUTTON, cancelButton);
            cancelButton.setOnAction(e -> {
                processCancel();
                });
        grid.addRow(0, textDialogLabel, textDialogField);
        grid.addRow(1, okButton, cancelButton);
        
        
    }

    private void processOkay() {
        if(textDialogField.getText().trim().length() > 0){
            inputText = textDialogField.getText();
        } else {
            inputText = null;
        }
        this.hide();
        textDialogField.clear();
        
    }

    private void processCancel() {
        inputText = null;
        this.hide();
        textDialogField.clear();
    }
    
    public String getInputText(){
        return inputText;
    }
    
    
}
