/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;

import djf.modules.AppLanguageModule;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CHANGE_LOGO_HEIGHT_LABEL;
import static gologolo.GoLogoLoPropertyType.CHANGE_LOGO_SIZE_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.CHANGE_LOGO_SIZE_OKAY_BUTTON;
import static gologolo.GoLogoLoPropertyType.CHANGE_LOGO_WIDTH_LABEL;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_BUTTON;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_GRID;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT_FIELD;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author doov
 */
public class ResizeLogoDialog extends Stage{

    
    class LogoSize {
            private double width;
            private double height;

            public LogoSize(double width, double height) {
                this.width = width;
                this.height = height;
            }
            public double getWidth() {return width;}
            public double getHeight() {return height;}
        }
    GoLogoLoApp app;
    GridPane grid;
    TextField logoWidthField;
    TextField logoHeightField;
    Pane canvas;
    LogoSize currentSize;
    public ResizeLogoDialog(Pane canvas, GoLogoLoApp app) {
        this.app = app;
        this.canvas = canvas;
        initDialog();
        currentSize = new LogoSize(800, 800);
        
          Scene scene = new Scene(grid);
        this.setScene(scene);
        
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_LOGO_DIALOG_GRID);
        
        
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }

    private void initDialog() {
        grid = new GridPane();
            grid.getStyleClass().add(CLASS_LOGO_DIALOG_GRID);
        
        AppLanguageModule languageSettings = app.getLanguageModule();    
        Label logoWidthLabel = new Label();
            logoWidthLabel.getStyleClass().add(CLASS_LOGO_TEXT);
            languageSettings.addLabeledControlProperty(CHANGE_LOGO_WIDTH_LABEL + "_TEXT", ((Labeled)logoWidthLabel).textProperty());
        logoWidthField = new TextField();
            logoWidthField.getStyleClass().add(CLASS_LOGO_TEXT_FIELD);
            
        Label logoHeightLabel = new Label();
            logoHeightLabel.getStyleClass().add(CLASS_LOGO_TEXT);
            languageSettings.addLabeledControlProperty(CHANGE_LOGO_HEIGHT_LABEL + "_TEXT", ((Labeled) logoHeightLabel).textProperty());
        logoHeightField = new TextField();
            logoHeightField.getStyleClass().add(CLASS_LOGO_TEXT_FIELD);    
            
        Button okButton = new Button();
            languageSettings.addLabeledControlProperty(CHANGE_LOGO_SIZE_OKAY_BUTTON + "_TEXT", ((Labeled)okButton).textProperty());
            okButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(CHANGE_LOGO_SIZE_OKAY_BUTTON, okButton);
            okButton.setOnAction(e -> {
                processOkay();
                });
        Button cancelButton = new Button();
            languageSettings.addLabeledControlProperty(CHANGE_LOGO_SIZE_CANCEL_BUTTON + "_TEXT", ((Labeled)cancelButton).textProperty());
            cancelButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(CHANGE_LOGO_SIZE_CANCEL_BUTTON, cancelButton);
            cancelButton.setOnAction(e -> {
                processCancel();
                });
             
        grid.addRow(0, logoWidthLabel, logoWidthField);
        grid.addRow(1, logoHeightLabel, logoHeightField);
        grid.addRow(2, okButton, cancelButton);
    }
    
    public void showResizeDialog(){
        logoWidthField.setText(""+currentSize.getWidth());
        logoHeightField.setText(""+currentSize.getHeight());
        showAndWait();
        
    }
    
    

    private void processOkay() {
        double width = Double.parseDouble(logoWidthField.getText());
        double height = Double.parseDouble(logoHeightField.getText());
        if(width > 0 && height > 0){
            currentSize = new LogoSize(width, height);
            resizeLogo();
        }
        hide();
    }

    private void processCancel() {
        hide();
    }
    
    private void resizeLogo() {
        canvas.setClip(new Rectangle(currentSize.getWidth(), currentSize.getHeight()));
    }
    
}
