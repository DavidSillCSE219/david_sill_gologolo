/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialogs;

import djf.modules.AppLanguageModule;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CHANGE_ITEM_NAME_LABEL;
import static gologolo.GoLogoLoPropertyType.CHANGE_NAME_DIALOG_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.CHANGE_NAME_DIALOG_OKAY_BUTTON;
import static gologolo.GoLogoLoPropertyType.EDIT_TEXT_DIALOG_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.EDIT_TEXT_DIALOG_OKAY_BUTTON;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_BUTTON;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_DIALOG_GRID;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT_FIELD;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author doov
 */
public class ChangeItemNameDialog extends Stage {
    
    GoLogoLoApp app;
    GridPane grid;
    TextField textDialogField;
    String newName;
    
    public ChangeItemNameDialog(GoLogoLoApp app) {
        this.app = app;

        initDialog();
        
        Scene scene = new Scene(grid);
        this.setScene(scene);
                                  
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_LOGO_DIALOG_GRID);
        
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    public void showChangeNameDialog(String currentName){
        newName = null;
        textDialogField.setText(currentName);
        showAndWait();
    }
    
    public String getNewName(){
        return this.newName;
    }
    


    private void initDialog() {
        grid = new GridPane();
            grid.getStyleClass().add(CLASS_LOGO_DIALOG_GRID);
        
        AppLanguageModule languageSettings = app.getLanguageModule();    
        Label textDialogLabel = new Label();
            textDialogLabel.getStyleClass().add(CLASS_LOGO_TEXT);
            languageSettings.addLabeledControlProperty(CHANGE_ITEM_NAME_LABEL + "_TEXT", ((Labeled)textDialogLabel).textProperty());
        textDialogField = new TextField();
            textDialogField.getStyleClass().add(CLASS_LOGO_TEXT_FIELD);
            
        Button okButton = new Button();
            languageSettings.addLabeledControlProperty(CHANGE_NAME_DIALOG_OKAY_BUTTON + "_TEXT", ((Labeled)okButton).textProperty());
            okButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(CHANGE_NAME_DIALOG_OKAY_BUTTON, okButton);
            okButton.setOnAction(e -> {
                processOkay();
                });
        Button cancelButton = new Button();
            languageSettings.addLabeledControlProperty(CHANGE_NAME_DIALOG_CANCEL_BUTTON + "_TEXT", ((Labeled)cancelButton).textProperty());
            cancelButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
            app.getGUIModule().addGUINode(CHANGE_NAME_DIALOG_CANCEL_BUTTON, cancelButton);
            cancelButton.setOnAction(e -> {
                processCancel();
                });
             
        grid.addRow(0, textDialogLabel, textDialogField);
        grid.addRow(1, okButton, cancelButton);
        
        
    }

    private void processOkay() {
        if(textDialogField.getText().trim().length() > 0){
            newName = textDialogField.getText();
        } else {
            newName = null;
        }
        this.hide();
        textDialogField.clear();
    }

    private void processCancel() {
        newName = null;
        this.hide();
        textDialogField.clear();
    }
}
