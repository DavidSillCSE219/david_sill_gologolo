/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace;


import djf.components.AppWorkspaceComponent;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_DJF_TOOLBAR_PANE;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_COLOR_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_CONTROL_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_RADIUS_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_RADIUS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_THICKNESS_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_BORDER_THICKNESS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_CANVAS;
import static gologolo.GoLogoLoPropertyType.LOGO_DELETE_SHAPE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_CONTROLS_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SELECT_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SELECT_COMBO_BOX_DEFAULT_VALUE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SELECT_COMBO_BOX_OPTIONS;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SELECT_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SIZE_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SIZE_COMBO_BOX_DEFAULT_VALUE;
import static gologolo.GoLogoLoPropertyType.LOGO_FONT_SIZE_COMBO_BOX_OPTIONS;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_SIZE_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_SIZE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_NAME_COLUMN;
import static gologolo.GoLogoLoPropertyType.LOGO_TABLE_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_NEW_CIRCLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_NEW_IMAGE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_NEW_RECTANGLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_NEW_TEXT_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_NEW_TRIANGLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_ORDER_COLUMN;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_X_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_X_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_Y_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CENTER_Y_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CONTROL_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX_DEFAULT_VALUE;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX_OPTIONS;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_CYCLE_METHOD_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_RADIUS_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_RADIUS_SLIDER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_0_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_0_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_1_COLOR_PICKER;
import static gologolo.GoLogoLoPropertyType.LOGO_RADIAL_GRADIENT_STOP_1_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_SHAPE_TABLE;
import static gologolo.GoLogoLoPropertyType.LOGO_SHAPE_TABLE_DOWN_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_SHAPE_TABLE_EDIT_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_SHAPE_TABLE_UP_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TABLE_CONTROLS_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_TEXT_BIGGER_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TEXT_BOLD_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TEXT_ITALIC_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TEXT_SMALLER_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TEXT_UNDERLINE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TOOLBAR_HOME_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TOOLBAR_RESIZE_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TOOLBAR_SNAP_CHECKBOX;
import static gologolo.GoLogoLoPropertyType.LOGO_TOOLBAR_ZOOM_IN_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TOOLBAR_ZOOM_OUT_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_TYPE_COLUMN;
import static gologolo.GoLogoLoPropertyType.LOGO_WORKSPACE_CONTROLS_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_WORKSPACE_SHAPE_CONTROLS_PANE;
import gologolo.data.GoLogoLoData;
import gologolo.data.logoitems.LogoItem;
import gologolo.workspace.controllers.ItemsController;
import gologolo.workspace.controllers.RadialGradientController;
import gologolo.workspace.dialogs.ResizeLogoDialog;
import gologolo.workspace.foolproof.GoLogoLoFoolproofDesign;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BOX;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BOX_CONTROLS;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BOX_SPACED;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BOX_TABLE;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_BUTTON;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_COLUMN;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_PANE;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_SLIDER;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_SMALL_HEADER;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TABLE;
import static gologolo.workspace.style.GoLogoLoStyle.CLASS_LOGO_TEXT;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.CycleMethod;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Transform;
import properties_manager.PropertiesManager;

/**
 *
 * @author doov
 */
public class GoLogoLoWorkspace extends AppWorkspaceComponent{
    
    private TableView table;
    private Pane canvas;
    private ItemsController itemController;
    private GoLogoLoFoolproofDesign foolproof;
    private ResizeLogoDialog resizeDialog;
    private RadialGradientController gradientController;
    public boolean settingValue;
    
    public GoLogoLoWorkspace(GoLogoLoApp initApp) {
        super(initApp);
        settingValue = false;
        gradientController = new RadialGradientController();
        initLayout();
        foolproof = new GoLogoLoFoolproofDesign(initApp);
        
    }
    
    private void initLayout(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        AppNodesBuilder builder = app.getGUIModule().getNodesBuilder();
        
        //This Is the left side of the UI, including the table and table controls for the layering of shapes
        VBox tablePane = builder.buildVBox(LOGO_TABLE_PANE, null, null, CLASS_LOGO_BOX_TABLE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        table = builder.buildTableView(LOGO_SHAPE_TABLE, tablePane, null, CLASS_LOGO_TABLE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            System.out.println(table.getMaxHeight());
            TableColumn orderColumn  = builder.buildTableColumn(LOGO_ORDER_COLUMN, table, CLASS_LOGO_COLUMN);                orderColumn.setCellFactory(new PropertyValueFactory<LogoItem, String>("name"));
//                orderColumn.setCellValueFactory(new PropertyValueFactory<LogoItem, String>("order"));
            TableColumn nameColumn   = builder.buildTableColumn(LOGO_NAME_COLUMN,table, CLASS_LOGO_COLUMN);
//                nameColumn.setCellValueFactory(new PropertyValueFactory<LogoItem, String>("name"));
            TableColumn typeColumn   = builder.buildTableColumn(LOGO_TYPE_COLUMN, table, CLASS_LOGO_COLUMN);
//                typeColumn.setCellValueFactory(new PropertyValueFactory<LogoItem, String>("type"));
            table.getColumns().setAll(orderColumn, nameColumn, typeColumn);
            
        //Table Controls    
        HBox tableControlsPane = builder.buildHBox(LOGO_TABLE_CONTROLS_PANE, tablePane, null, CLASS_LOGO_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button shapeTableUpButton = builder.buildIconButton(LOGO_SHAPE_TABLE_UP_BUTTON, tableControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button shapeTableDownButton = builder.buildIconButton(LOGO_SHAPE_TABLE_DOWN_BUTTON, tableControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button shapeTableEditButton = builder.buildIconButton(LOGO_SHAPE_TABLE_EDIT_BUTTON, tableControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        //Toolbar Controls
        ToolBar logoToolBar =  new ToolBar();
        
        Button homeButton = builder.buildIconButton(LOGO_TOOLBAR_HOME_BUTTON, null, logoToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomInButton = builder.buildIconButton(LOGO_TOOLBAR_ZOOM_IN_BUTTON, null, logoToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomOutButton = builder.buildIconButton(LOGO_TOOLBAR_ZOOM_OUT_BUTTON, null, logoToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button resizeButton = builder.buildIconButton(LOGO_TOOLBAR_RESIZE_BUTTON, null, logoToolBar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        CheckBox snapBox = builder.buildCheckBox(LOGO_TOOLBAR_SNAP_CHECKBOX, null, logoToolBar, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        logoToolBar.getStyleClass().add(CLASS_DJF_TOOLBAR_PANE);
        app.getGUIModule().getTopToolbarPane().getChildren().add(3, logoToolBar);
        
        //Canvas for drawing on
        
        canvas = new Pane();
        app.getGUIModule().addGUINode(LOGO_CANVAS, canvas);
            canvas.setClip(new Rectangle(800, 800));
            resizeDialog = new ResizeLogoDialog(canvas, (GoLogoLoApp)app);
        Pane canvasPane = new Pane();
            canvasPane.setClip(new Rectangle(1600, 1000));
            canvasPane.getChildren().add(canvas);
        
        VBox controlsPane = builder.buildVBox(LOGO_WORKSPACE_CONTROLS_PANE, null, null, CLASS_LOGO_BOX_CONTROLS, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        HBox shapeControlsPane = builder.buildHBox(LOGO_WORKSPACE_SHAPE_CONTROLS_PANE, controlsPane, null, CLASS_LOGO_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button newTexButton = builder.buildIconButton(LOGO_NEW_TEXT_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button newImageButton = builder.buildIconButton(LOGO_NEW_IMAGE_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button newRectangleButton = builder.buildIconButton(LOGO_NEW_RECTANGLE_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button newCircleButton = builder.buildIconButton(LOGO_NEW_CIRCLE_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button newTriangleButton = builder.buildIconButton(LOGO_NEW_TRIANGLE_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button deleteShapeButton = builder.buildIconButton(LOGO_DELETE_SHAPE_BUTTON, shapeControlsPane, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        VBox fontPane = builder.buildVBox(LOGO_FONT_PANE, controlsPane, null, CLASS_LOGO_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        HBox fontSelectPane = builder.buildHBox(LOGO_FONT_SELECT_PANE, fontPane, null, CLASS_LOGO_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            ComboBox fontSelect = builder.buildComboBox(LOGO_FONT_SELECT_COMBO_BOX, LOGO_FONT_SELECT_COMBO_BOX_OPTIONS, LOGO_FONT_SELECT_COMBO_BOX_DEFAULT_VALUE, fontSelectPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            ComboBox fontSize = builder.buildComboBox(LOGO_FONT_SIZE_COMBO_BOX, LOGO_FONT_SIZE_COMBO_BOX_OPTIONS, LOGO_FONT_SIZE_COMBO_BOX_DEFAULT_VALUE, fontSelectPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        HBox fontControls = builder.buildHBox(LOGO_FONT_CONTROLS_PANE, fontPane, null, CLASS_LOGO_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button textBoldButton = builder.buildIconButton(LOGO_TEXT_BOLD_BUTTON, fontControls, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button textItalicButton = builder.buildIconButton(LOGO_TEXT_ITALIC_BUTTON, fontControls, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button textSmallerButton = builder.buildIconButton(LOGO_TEXT_SMALLER_BUTTON, fontControls, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button textBiggerButton = builder.buildIconButton(LOGO_TEXT_BIGGER_BUTTON, fontControls, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Button textUnderlineButton = builder.buildIconButton(LOGO_TEXT_UNDERLINE_BUTTON, fontControls, null, CLASS_LOGO_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        VBox borderControlPane = builder.buildVBox(LOGO_BORDER_CONTROL_PANE, controlsPane, null, CLASS_LOGO_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Label logoItemSize = builder.buildLabel(LOGO_ITEM_SIZE_LABEL, borderControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Slider logoItemSizeSlider = builder.buildSlider(LOGO_ITEM_SIZE_SLIDER, borderControlPane, null, CLASS_LOGO_SLIDER, 20, 200, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Label borderThicknessLabel = builder.buildLabel(LOGO_BORDER_THICKNESS_LABEL, borderControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Slider borderThicknessSlider = builder.buildSlider(LOGO_BORDER_THICKNESS_SLIDER, borderControlPane, null, CLASS_LOGO_SLIDER, 0, 50, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Label borderColorLabel = builder.buildLabel(LOGO_BORDER_COLOR_LABEL, borderControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            ColorPicker borderColorPicker = builder.buildColorPicker(LOGO_BORDER_COLOR_PICKER, borderControlPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Label borderRadiusLabel = builder.buildLabel(LOGO_BORDER_RADIUS_LABEL, borderControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
            Slider borderRadiusSlider = builder.buildSlider(LOGO_BORDER_RADIUS_SLIDER, borderControlPane, null, CLASS_LOGO_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        VBox radialGradientControlPane = builder.buildVBox(LOGO_RADIAL_GRADIENT_CONTROL_PANE, controlsPane, null, CLASS_LOGO_BOX_SPACED, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Label radialGradientControlLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_LABEL, radialGradientControlPane, null, CLASS_LOGO_SMALL_HEADER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Label radialGradientFocusAngleLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Slider radialGradientFocusAngleSlider = builder.buildSlider(LOGO_RADIAL_GRADIENT_FOCUS_ANGLE_SLIDER, radialGradientControlPane, null, CLASS_LOGO_SLIDER, 0, 360, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                    radialGradientFocusAngleSlider.setSnapToTicks(false);
                Label radialGradientFocusDistanceLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Slider radialGradientFocusDistanceSlider = builder.buildSlider(LOGO_RADIAL_GRADIENT_FOCUS_DISTANCE_SLIDER, radialGradientControlPane, null, CLASS_LOGO_SLIDER, -1, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                    radialGradientFocusDistanceSlider.setSnapToTicks(false);
                Label radialGradientCenterXLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_CENTER_X_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Slider radialGradientCenterXSlider = builder.buildSlider(LOGO_RADIAL_GRADIENT_CENTER_X_SLIDER, radialGradientControlPane, null, CLASS_LOGO_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                    radialGradientCenterXSlider.setSnapToTicks(false);
                Label radialGradientCenterYLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_CENTER_Y_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Slider radialGradientCenterYSlider = builder.buildSlider(LOGO_RADIAL_GRADIENT_CENTER_Y_SLIDER, radialGradientControlPane, null, CLASS_LOGO_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                    radialGradientCenterYSlider.setSnapToTicks(false);
                Label radialGradientRadiusLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_RADIUS_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Slider radialGradientRadiusSlider = builder.buildSlider(LOGO_RADIAL_GRADIENT_RADIUS_SLIDER, radialGradientControlPane, null, CLASS_LOGO_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                    radialGradientRadiusSlider.setSnapToTicks(false);
                Label radialGradientCycleMethodLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_CYCLE_METHOD_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                ComboBox radialGradientCycleMethodComboBox = builder.buildComboBox(LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX, LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX_OPTIONS, LOGO_RADIAL_GRADIENT_CYCLE_METHOD_COMBO_BOX_DEFAULT_VALUE, radialGradientControlPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Label radialGradientStop0ColorLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_STOP_0_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                ColorPicker radialGradientStop0ColorPicker = builder.buildColorPicker(LOGO_RADIAL_GRADIENT_STOP_0_COLOR_PICKER, radialGradientControlPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                Label radialGradientStop1ColorLabel = builder.buildLabel(LOGO_RADIAL_GRADIENT_STOP_1_LABEL, radialGradientControlPane, null, CLASS_LOGO_TEXT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                ColorPicker radialGradientStop1ColorPicker = builder.buildColorPicker(LOGO_RADIAL_GRADIENT_STOP_1_COLOR_PICKER, radialGradientControlPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

            
        workspace = new BorderPane();
        ((BorderPane)workspace).setLeft(tablePane);
        ((BorderPane)workspace).setCenter(canvasPane);
        ((BorderPane)workspace).setRight(controlsPane);
        
        //Setting button Actions
        
        itemController = new ItemsController((GoLogoLoApp)app);
        
        zoomInButton.setOnAction(e -> {
            Scale scale = new Scale(1.2, 1.2);
            canvas.getTransforms().add(scale);
        });
        
        zoomOutButton.setOnAction(e -> {
            ObservableList<Transform> trans = canvas.getTransforms();
            if(trans.size() > 0){
                canvas.getTransforms().remove(canvas.getTransforms().size() - 1);
            }
        });
        homeButton.setOnAction(e -> {
            canvas.getTransforms().clear();
            canvas.setTranslateX(0);
            canvas.setTranslateY(0);
        });
        
        shapeTableDownButton.setOnAction(e ->{
            itemController.moveSelectedItemDown();
        });
        
        shapeTableUpButton.setOnAction(e -> {
            itemController.moveSelectedItemUp();
        });
        shapeTableEditButton.setOnAction(e -> {
            itemController.changeItemName();
        });
        
        resizeButton.setOnAction(e ->{
            resizeDialog.showResizeDialog();
            app.getFileModule().markAsEdited(true);
        });
        snapBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                itemController.toggleGridSnap(newValue);
            }
            
        });
        
        fontSelect.setOnAction(e -> {
            if(!settingValue){
                itemController.setFont(fontSelect.getValue().toString());
            }
        });
        fontSize.setOnAction(e -> {
            if(!settingValue){
                itemController.setFontSize(Integer.parseInt(fontSize.getValue().toString()));
            }
        });
        
        textBoldButton.setOnAction(e -> {
            itemController.toggleSelectedBold();
        });
        textItalicButton.setOnAction(e -> {
            itemController.toggleSelectedItalic();
        });
        textSmallerButton.setOnAction(e -> {
            itemController.decreaseSelectedFontSize();
        });
        textBiggerButton.setOnAction(e -> {
            itemController.increaseSelectedFontSize();
        });
        textUnderlineButton.setOnAction(e -> {
            itemController.underLineText();
        });
        
        borderThicknessSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    itemController.setItemBorderThickness(borderThicknessSlider.getValue(), false);
                }
            }
        });
        
        
        logoItemSizeSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    itemController.changeItemSize(logoItemSizeSlider.getValue(), false);
                }
            }
        });
        
        borderColorPicker.setOnAction(e -> {
            itemController.setItemBorderColor(borderColorPicker.getValue());
        });
        borderRadiusSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    itemController.setItemBorderRadius(borderRadiusSlider.getValue(), false);
                }
            }
        });
       
        
        
        newRectangleButton.setOnAction(e ->{
            itemController.addRectangle();
        });
        newCircleButton.setOnAction(e -> {
            itemController.addCircle();
        });
        newTexButton.setOnAction(e -> {
            itemController.addText();
        });
        newImageButton.setOnAction(e -> {
            itemController.addImage();
        });
        deleteShapeButton.setOnAction(e -> {
            itemController.deleteSelected();
        });
        
        radialGradientFocusAngleSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setFocusAngle(radialGradientFocusAngleSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                } 
            }
        });
        radialGradientFocusDistanceSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setFocusDistance(radialGradientFocusDistanceSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                }
            }
        });
        radialGradientCenterXSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setCenterX(radialGradientCenterXSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                }
            }
        });
        radialGradientCenterYSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setCenterY(radialGradientCenterYSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                }
            }
        });
        radialGradientRadiusSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setRadius(radialGradientRadiusSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                }  
            }
        });
        radialGradientFocusAngleSlider.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(!settingValue){
                    gradientController.setFocusAngle(radialGradientFocusAngleSlider.getValue());
                    itemController.updateGradient(gradientController.getGradient());
                } 
            }
        });
        
        radialGradientCycleMethodComboBox.setOnAction(e -> {
            if(!settingValue){
                gradientController.setCycleMethod(CycleMethod.valueOf(radialGradientCycleMethodComboBox.getValue().toString()));
                itemController.updateGradient(gradientController.getGradient());
            }
                
            });
        
        radialGradientStop0ColorPicker.setOnAction(e -> {
            if(!settingValue){
                gradientController.setStop0(radialGradientStop0ColorPicker.getValue());
                itemController.updateGradient(gradientController.getGradient());
            }
        });
        
        radialGradientStop1ColorPicker.setOnAction(e -> {
            if(!settingValue){
                gradientController.setStop1(radialGradientStop1ColorPicker.getValue());
                itemController.updateGradient(gradientController.getGradient());
            }
        });
        
        canvas.setOnMouseClicked(e -> {
            itemController.clearSelected();
        });
        controlsPane.setOnMouseClicked(e -> {
            itemController.clearSelected();
        });
        itemController.getDragController().makeMiddleMouseDraggable(canvas);
    }
    
    public Pane getCanvas(){
        return this.canvas;
    }
    
    public RadialGradientController getGradientController(){
        return this.gradientController;
    }
    
    public TableView getTable(){
        return this.table;
    }

    public ItemsController getItemController() {
        return itemController;
    }
    
    public GoLogoLoFoolproofDesign getLogoFoolProofDesign() {
        return this.foolproof;
    }
    
    
    public void updateCanvas(){
        canvas.getChildren().clear();
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        
        for(Node item : data.getLogoItems()){
            canvas.getChildren().add(item);
        }
        app.getFileModule().markAsEdited(true);
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        
    }
    
}
