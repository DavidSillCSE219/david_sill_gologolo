/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.style;

/**
 *
 * @author doov
 */
public class GoLogoLoStyle {
    
    public static final String CLASS_LOGO_TOOLBAR_PANE          = "logo_toolbar_pane";
    public static final String CLASS_LOGO_PANE                  = "logo_pane";
    public static final String CLASS_LOGO_BOX                   = "logo_box";
    public static final String CLASS_LOGO_BOX_SPACED            = "logo_box_spaced";
    public static final String CLASS_LOGO_BOX_TABLE             = "logo_box_table";  
    public static final String CLASS_LOGO_BOX_CONTROLS          = "logo_box_controls";
    public static final String CLASS_LOGO_BIG_HEADER            = "logo_big_header";
    public static final String CLASS_LOGO_SMALL_HEADER          = "logo_small_header";
    public static final String CLASS_LOGO_TABLE                 = "logo_table";
    public static final String CLASS_LOGO_BUTTON                = "logo_icon_button";
    public static final String CLASS_LOGO_TEXT                  = "logo_text";
    public static final String CLASS_LOGO_SLIDER                = "logo_slider";
    public static final String CLASS_LOGO_COLUMN                = "logo_column-header";
    public static final String CLASS_LOGO_TEXT_FIELD            = "lodo_text"; //TODO add CSS
    public static final String CLASS_LOGO_DIALOG_BUTTON         = "logo_dialog_button";
    public static final String CLASS_LOGO_DIALOG_GRID           = "logo_dialog_grid";
    
    
    public static final String CLASS_LOGO_HIGHLIGHT             = "logo_highlight";
    public static final String CLASS_LOGO_BOLD                  = "logo_bold";
    public static final String CLASS_LOGO_ITALIC                = "logo_italic";
    
    
    
    
    
}
