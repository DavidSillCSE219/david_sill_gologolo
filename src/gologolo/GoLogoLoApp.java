/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo;

import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.components.AppWorkspaceComponent;
import gologolo.clipboard.GoLogoLoClipboard;
import gologolo.data.GoLogoLoData;
import gologolo.files.GoLogoLoFiles;
import gologolo.workspace.GoLogoLoWorkspace;
import java.util.Locale;
import static javafx.application.Application.launch;

/**
 *
 * @author David Sill
 *          111486160
 */
public class GoLogoLoApp extends AppTemplate {
    
        public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }

    @Override
    public AppClipboardComponent buildClipboardComponent(AppTemplate app) {
        return new GoLogoLoClipboard(this);
    }

    @Override
    public AppDataComponent buildDataComponent(AppTemplate app) {
        return new GoLogoLoData(this);
    }

    @Override
    public AppFileComponent buildFileComponent() {
        return new GoLogoLoFiles();
    }

    @Override
    public AppWorkspaceComponent buildWorkspaceComponent(AppTemplate app) {
        return new GoLogoLoWorkspace(this);
    }
    
}
